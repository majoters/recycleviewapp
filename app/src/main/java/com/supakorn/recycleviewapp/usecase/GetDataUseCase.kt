package com.supakorn.recycleviewapp.usecase

import com.supakorn.recycleviewapp.model.Data
import com.supakorn.recycleviewapp.model.Posts
import com.supakorn.recycleviewapp.model.Users
import com.supakorn.recycleviewapp.networking.Resource
import com.supakorn.recycleviewapp.networking.ResponseHandler
import com.supakorn.recycleviewapp.repository.DataRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.zip

class GetDataUseCase(
    private val dataRepository: DataRepository,
    private val responseHandler: ResponseHandler

) {
    fun execute(): Flow<Resource<List<Data>>> {
        return dataRepository.getPost().zip(dataRepository.getUser()) { posts, users ->
            responseHandler.handleSuccess(mapModel(posts, users))
        }.catch { error ->
            emit(responseHandler.handleException(error))
        }
    }

    private fun mapModel(posts: Posts?, users: Users?): List<Data> {
        val dataList = mutableListOf<Data>()
        posts?.forEach { post ->
            users?.forEach { user ->
                if (post.userId == user.id) {
                    val data = Data().apply {
                        id = post.id
                        title = post.title
                        name = user.name
                        email = user.email
                        body = post.body
                    }
                    dataList.add(data)
                }
            }
        }
        return dataList.toList()
    }
}
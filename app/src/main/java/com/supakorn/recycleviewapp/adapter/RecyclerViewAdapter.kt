package com.supakorn.recycleviewapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.supakorn.recycleviewapp.R
import com.supakorn.recycleviewapp.databinding.ItemLayoutBinding
import com.supakorn.recycleviewapp.model.Data
import kotlinx.android.synthetic.main.item_layout.view.*


class RecyclerViewAdapter :
    RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder>() {
    var itemClickListener: ((item: Data, position: Int) -> Unit)? = null
    private var dataArrayList = mutableListOf<Data>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        // Inflate Layout
        val binding = ItemLayoutBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return RecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        // Set the data to textview and imageview.
        val recyclerData: Data = dataArrayList[position]
        holder.bind(recyclerData) {
            itemClickListener?.invoke(dataArrayList[position], position)
        }
    }

    override fun getItemCount(): Int {
        // this method returns the size of recyclerview
        return dataArrayList.size
    }

    fun setDataList(dataList: List<Data>) {
        dataArrayList.clear()
        dataArrayList.addAll(dataList)
        notifyDataSetChanged()
    }

    fun sortDescending(state: Boolean) {
        when(state) {
            true -> dataArrayList.sortByDescending { it.id }
            false -> dataArrayList.sortBy { it.id }
        }
        notifyDataSetChanged()
    }

    // View Holder Class to handle Recycler View.
    inner class RecyclerViewHolder(private val binding: ItemLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(
            item: Data,
            itemClickListener: (() -> Unit)?
        ) = with(binding) {
            titleTextView.text = "Title: ${item.title}"
            nameTextView.text = "Name: ${item.name}"
            emailTextView.text = "Email: ${item.email}"
            root.setOnClickListener {
                itemClickListener?.invoke()
            }
        }
    }

}
package com.supakorn.recycleviewapp.model

class Data {
    var id: Int = 0
    var title: String? = ""
    var name: String? = ""
    var email: String? = ""
    var body: String? = ""
}
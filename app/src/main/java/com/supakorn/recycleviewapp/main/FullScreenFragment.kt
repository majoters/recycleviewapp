package com.supakorn.recycleviewapp.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.supakorn.recycleviewapp.databinding.FullScreenFragmentBinding
import com.supakorn.recycleviewapp.model.Data

class FullScreenFragment : Fragment() {

    private lateinit var binding: FullScreenFragmentBinding
    private lateinit var data: Data

    companion object {
        fun newInstance(item: Data): FullScreenFragment {
            return FullScreenFragment().apply {
                data = item
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FullScreenFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialLayout()
    }

    @SuppressLint("SetTextI18n")
    private fun initialLayout() {
        binding.titleTextViewFS.text = "Title: ${data.title}"
        binding.bodyTextViewFS.text = "Body: ${data.body}"
    }

}
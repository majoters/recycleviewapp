package com.supakorn.recycleviewapp.main

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.supakorn.recycleviewapp.R
import com.supakorn.recycleviewapp.adapter.RecyclerViewAdapter
import com.supakorn.recycleviewapp.databinding.MainFragmentBinding
import com.supakorn.recycleviewapp.networking.Status
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: MainFragmentBinding
    private val dataAdapter by lazy {
        RecyclerViewAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initialLayout()
        initialViewModel()
        viewModel.getData()
    }

    private fun initialLayout() {
        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = dataAdapter
        }
        binding.ascendigBtn.setOnClickListener { dataAdapter.sortDescending(false) }
        binding.descendingBtn.setOnClickListener { dataAdapter.sortDescending(true) }
        dataAdapter.itemClickListener = { item, _ ->
            val fragment = FullScreenFragment.newInstance(item)
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.containerView, fragment, "secondFragment")
                ?.commit();
        }
    }

    private fun initialViewModel() {
        viewModel.onGetData().observe(viewLifecycleOwner, {
            when (it.status) {
                Status.SUCCESS -> {
                    it.data?.let { dataList -> dataAdapter.setDataList(dataList) }
                }
                Status.ERROR -> it.message?.let { message -> showError(message) }
                Status.LOADING -> showLoading()
            }
        })
        viewModel.onShowLoading().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
        })
        viewModel.onHideLoading().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.GONE
            binding.recyclerView.visibility = View.VISIBLE
        })
        viewModel.onErrorData().observe(viewLifecycleOwner, {
            binding.textStatus.visibility = View.VISIBLE
            binding.recyclerView.visibility = View.GONE
            binding.textStatus.text = it
        })
    }

    @SuppressLint("SetTextI18n")
    private fun showLoading() {
        binding.textStatus.text = "Loading..."
    }

    @SuppressLint("SetTextI18n")
    private fun showError(message: String) {
        binding.textStatus.visibility = View.VISIBLE
        binding.recyclerView.visibility = View.GONE
        binding.textStatus.text = "Error: $message"
    }

}
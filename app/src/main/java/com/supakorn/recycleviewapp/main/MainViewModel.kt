package com.supakorn.recycleviewapp.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.supakorn.recycleviewapp.model.Data
import com.supakorn.recycleviewapp.networking.Resource
import com.supakorn.recycleviewapp.usecase.GetDataUseCase
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch

class MainViewModel(
    private val getDataUseCase: GetDataUseCase
) : ViewModel() {
    fun onGetData(): LiveData<Resource<List<Data>>> = data
    fun onErrorData(): LiveData<String> = error
    fun onShowLoading(): LiveData<Unit> = loading
    fun onHideLoading(): LiveData<Unit> = loaded

    var data = MutableLiveData<Resource<List<Data>>>()
    var error = MutableLiveData<String>()
    var loading = MutableLiveData<Unit>()
    var loaded = MutableLiveData<Unit>()

    fun getData() {
        viewModelScope.launch {
            getDataUseCase.execute()
                .onStart { loading.value = Unit }
                .catch { exception ->
                    loaded.value = Unit
                    error.value = exception.message
                }
                .collect {
                    loaded.value = Unit
                    data.value = it
                }
        }
    }
}
package com.supakorn.recycleviewapp.networking

import androidx.annotation.WorkerThread
import com.supakorn.recycleviewapp.model.Posts
import com.supakorn.recycleviewapp.model.Users
import retrofit2.http.GET

interface ApiInterface {

    @WorkerThread
    @GET("posts")
    suspend fun getPosts(): Posts

    @WorkerThread
    @GET("users")
    suspend fun getUsers(): Users
}
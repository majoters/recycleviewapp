package com.supakorn.recycleviewapp.networking

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
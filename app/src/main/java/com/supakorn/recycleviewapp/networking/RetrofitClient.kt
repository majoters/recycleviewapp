package com.supakorn.recycleviewapp.networking

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

fun okHttp(): OkHttpClient = OkHttpClient.Builder()
    .build()

fun retrofit(baseUrl: String): Retrofit = Retrofit.Builder()
    .callFactory(OkHttpClient.Builder().build())
    .baseUrl(baseUrl)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

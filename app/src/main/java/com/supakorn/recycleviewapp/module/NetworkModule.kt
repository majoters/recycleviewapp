package com.supakorn.recycleviewapp.module

import com.supakorn.recycleviewapp.networking.ApiInterface
import com.supakorn.recycleviewapp.networking.ResponseHandler
import com.supakorn.recycleviewapp.networking.okHttp
import com.supakorn.recycleviewapp.networking.retrofit
import org.koin.dsl.module
import retrofit2.Retrofit

const val BASE_URL = "https://jsonplaceholder.typicode.com/"

val networkModule = module {
    single {
        okHttp()
    }
    single {
        retrofit(BASE_URL)
    }
    single {
        get<Retrofit>().create(ApiInterface::class.java)
    }
    factory { ResponseHandler() }
}
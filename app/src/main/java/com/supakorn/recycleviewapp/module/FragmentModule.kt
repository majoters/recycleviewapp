package com.supakorn.recycleviewapp.module

import com.supakorn.recycleviewapp.main.FullScreenFragment
import com.supakorn.recycleviewapp.main.MainFragment
import org.koin.dsl.module

val fragmentModule = module {
    factory { MainFragment() }
    factory { FullScreenFragment() }
}
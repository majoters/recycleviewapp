package com.supakorn.recycleviewapp.module

import com.supakorn.recycleviewapp.repository.DataRepository
import com.supakorn.recycleviewapp.usecase.GetDataUseCase
import org.koin.dsl.module

val dataModule = module {
    factory { GetDataUseCase(get(), get()) }
    factory { DataRepository(get()) }
}
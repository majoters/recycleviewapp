package com.supakorn.recycleviewapp.repository

import com.supakorn.recycleviewapp.model.Posts
import com.supakorn.recycleviewapp.model.Users
import com.supakorn.recycleviewapp.networking.ApiInterface
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

open class DataRepository(
    private val apiInterface: ApiInterface
) {

    fun getPost(): Flow<Posts> {
        return flow {
            emit(apiInterface.getPosts())
        }
    }

    fun getUser(): Flow<Users> {
        return flow {
            emit(apiInterface.getUsers())
        }
    }
}
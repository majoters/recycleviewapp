package com.supakorn.recycleviewapp.usecase

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.supakorn.recycleviewapp.model.Posts
import com.supakorn.recycleviewapp.model.PostsItem
import com.supakorn.recycleviewapp.model.Users
import com.supakorn.recycleviewapp.model.UsersItem
import com.supakorn.recycleviewapp.networking.Resource
import com.supakorn.recycleviewapp.networking.ResponseHandler
import com.supakorn.recycleviewapp.repository.DataRepository
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class GetDataUseCaseTest {
    private lateinit var getDataUseCase: GetDataUseCase
    private lateinit var dataRepository: DataRepository
    private lateinit var responseHandler: ResponseHandler

    @Before
    fun setUp() {
        dataRepository = mock()
        responseHandler = mock()
        getDataUseCase = GetDataUseCase(dataRepository, responseHandler)
    }

    @Test
    fun `test getPost and getUser when usecase is executed, then response is returned`() = runBlockingTest {
        val postsItem = PostsItem(
            body = "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
            id = 1,
            title = "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            userId = 1
        )
        val posts: Posts? = null
        posts?.add(postsItem)
        posts?.add(postsItem)
        posts?.add(postsItem)
        val userItem = UsersItem(
            address = UsersItem.Address(
                city = "Gwenborough",
                geo = UsersItem.Address.Geo(lat = "-37.3159", lng = "81.1496"),
                street = "Kulas Light",
                suite = "Apt. 556",
                zipcode = "92998-3874"
            ),
            company = UsersItem.Company(
                bs = "harness real-time e-markets",
                catchPhrase = "Multi-layered client-server neural-net",
                name = "Romaguera-Crona"
            ),
            email = "Sincere@april.biz",
            id = 1,
            name = "Leanne Graham",
            phone = "1-770-736-8031 x56442",
            username = "Bret",
            website = "hildegard.org"
        )
        val users: Users? = null
        users?.add(userItem)
        users?.add(userItem)
        users?.add(userItem)
        whenever(dataRepository.getPost()).thenReturn(flow { Resource.success(posts) })
        whenever(dataRepository.getUser()).thenReturn(flow { Resource.success(users) })
        getDataUseCase.execute().onEach {
            assertEquals(3, it.data?.size)
        }.launchIn(this)
    }

    @Test
    fun `test getPost and getUser when usecase is executed, then response is return null`() = runBlockingTest {
        val posts: Posts? = null
        val users: Users? = null
        whenever(dataRepository.getPost()).thenReturn(flow { Resource.success(posts) })
        whenever(dataRepository.getUser()).thenReturn(flow { Resource.success(users) })
        getDataUseCase.execute().onEach {
            assertEquals(0, it.data?.size)
        }.launchIn(this)
    }

    @Test
    fun `test getPost and getUser when usecase is executed, then response is return exception`() = runBlockingTest {
        whenever(dataRepository.getPost()).thenReturn(flow { Resource.error("Something went wrong", null) })
        whenever(dataRepository.getUser()).thenReturn(flow { Resource.error("Something went wrong", null) })
        getDataUseCase.execute().onEach {
            assertEquals(0, it.data?.size)
        }.launchIn(this)
    }
}
package com.supakorn.recycleviewapp.networking

import com.nhaarman.mockitokotlin2.mock
import com.supakorn.recycleviewapp.model.Data
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException

class ResponseHandlerTest {
    lateinit var responseHandler: ResponseHandler

    @Before
    fun setUp() {
        responseHandler = ResponseHandler()
    }

    @Test
    fun `when exception code is 401 then return unauthorised`() {
        val httpException = HttpException(Response.error<Data>(401, mock()))
        val result = responseHandler.handleException<Data>(httpException)
        Assert.assertEquals("Unauthorised", result.message)
    }

    @Test
    fun `when timeout then return timeout error`() {
        val socketTimeoutException = SocketTimeoutException()
        val result = responseHandler.handleException<Data>(socketTimeoutException)
        Assert.assertEquals("Timeout", result.message)
    }
}